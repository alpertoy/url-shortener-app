import { ShortUrlService } from './../../services/short-url.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-short-utl',
  templateUrl: './short-utl.component.html',
  styleUrls: ['./short-utl.component.css']
})
export class ShortUtlComponent implements OnInit {

  urlName: string;
  urlShort: string;
  urlShortened: boolean;
  loading: boolean;
  showError: boolean;
  textError: string;

  constructor(private shortUrlService: ShortUrlService) {
    this.urlName = '';
    this.urlShort = '';
    this.urlShortened = false;
    this.loading = false;
    this.showError = false;
    this.textError = '';
  }

  ngOnInit(): void {
  }

  shortenUrl() {

    //Validate URL
    if(this.urlName === '') {
      this.error('Please enter a URL');

      return;
    }

    this.urlShortened = false;
    this.loading = true;

    setTimeout(() => {
      this.getShortenedUrl();
    }, 2000);
  }

  getShortenedUrl() {
    this.shortUrlService.getUrlShort(this.urlName).subscribe(data => {
      this.loading = false;
      this.urlShortened = true;
      this.urlShort = data.link;
    }, error => {
      this.loading = false;
      this.urlName = '';
      if(error.error.description === 'The value provided is invalid.') {
        this.error('Please enter a valid URL');
      }
    });
  }

  error(value: string) {
    this.showError = true;
      this.textError = value;

      //Show error for 4 seconds
      setTimeout(() => {
        this.showError = false;
      }, 4000)
  }

}
