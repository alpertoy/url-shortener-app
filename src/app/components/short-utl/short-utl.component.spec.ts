import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortUtlComponent } from './short-utl.component';

describe('ShortUtlComponent', () => {
  let component: ShortUtlComponent;
  let fixture: ComponentFixture<ShortUtlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShortUtlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortUtlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
