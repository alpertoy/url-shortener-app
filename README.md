# URL Shortener App

URL Shortener App is built in Angular where you can shorten URLS.

## Bitly API

Bitly API is used to shorten URLs in this app.

`https://bitly.com/`

## Live Demo

[Check here](https://alpertoy.gitlab.io/url-shortener-app/)
